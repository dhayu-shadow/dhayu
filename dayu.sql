-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 27, 2016 at 01:54 
-- Server version: 5.5.31
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dayu`
--
CREATE DATABASE IF NOT EXISTS `dayu` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `dayu`;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_karyawan`
--

CREATE TABLE IF NOT EXISTS `tbl_karyawan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_karyawan` varchar(200) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `url_foto` varchar(200) NOT NULL,
  `have_user_login` enum('true','false') NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(32) NOT NULL,
  `id_group` int(11) NOT NULL,
  `is_active` enum('true','false') NOT NULL,
  `insert_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `insert_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_karyawan`
--

INSERT INTO `tbl_karyawan` (`id`, `nama_karyawan`, `tanggal_lahir`, `id_jabatan`, `url_foto`, `have_user_login`, `username`, `password`, `id_group`, `is_active`, `insert_date`, `update_date`, `insert_by`, `update_by`) VALUES
(1, 'dayu', '1992-09-04', 1, '', 'true', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 'true', '2016-04-26 06:21:28', '2016-04-26 08:17:22', 1, 1),
(2, 'admin', '2016-04-06', 2, '', 'true', 'munjalindra', '70d7164effcc575b4099c88154eb792a', 2, 'true', '2016-04-27 03:13:17', '2016-04-27 07:13:18', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_koleksi_buku`
--

CREATE TABLE IF NOT EXISTS `tbl_koleksi_buku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_buku` varchar(200) NOT NULL,
  `deskripsi_buku` varchar(200) NOT NULL,
  `id_pengarang` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `url_image` varchar(200) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `insert_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `insert_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_master`
--

CREATE TABLE IF NOT EXISTS `tbl_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_master` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `is_active` enum('true','false') NOT NULL,
  `insert_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `insert_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_master`
--

INSERT INTO `tbl_master` (`id`, `id_master`, `name`, `is_active`, `insert_date`, `update_date`, `insert_by`, `update_by`) VALUES
(1, 1, 'master', 'true', '2016-04-27 04:14:19', '2016-04-27 04:14:20', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pelanggan`
--

CREATE TABLE IF NOT EXISTS `tbl_pelanggan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pelanggan` varchar(200) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `kota` int(11) NOT NULL,
  `nomor_telepon` varchar(20) NOT NULL,
  `pendidikan` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `insert_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `insert_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi_sewa_buku`
--

CREATE TABLE IF NOT EXISTS `tbl_transaksi_sewa_buku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_sewa` date NOT NULL,
  `tanggal_jatuh_tempo` date NOT NULL,
  `id_koleksi_buku` int(11) NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `harga_sewa` varchar(200) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `insert_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `insert_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
