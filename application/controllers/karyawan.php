<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class karyawan extends CI_Controller
  {
    public function __construct()
    {
      parent::__construct();
      $this->load->library('upload');
      $this->load->library('simpleauth');
      $this->load->model('karyawan_model','karyawan');
    }

    public function login()
    {
      $cekLogin = $this->simpleauth->check_after_login();

      if($cekLogin)
        redirect('karyawan/index');
      $this->load->view('karyawan/login');
    }

    public function do_login()
    {
      $username = $this->input->post('username',TRUE);
      $pass     = $this->input->post('password',TRUE);
      $login    = $this->simpleauth->login($username,$pass);
      $role     = $this->session->userdata('id_jabatan');

      if($login==TRUE){
        sukses("Autorisasi login anda berhasil, selamat datang di manajemen sistem admin anda!");
        redirect('karyawan/dashboard');
      }else{
        gagal("Maaf Login gagal, Silahkan cek username & password anda!");
        redirect('karyawan/login');
      }
    }

    public function do_logout()
    {
      $logout = $this->simpleauth->logout();

      if($logout){
        sukses("You successfully logout into our system");
        redirect('karyawan/login');
      }else{
        gagal("Maaf proses keluar sistem gagal!");
        redirect('karyawan/dashboard');
      }
    }

    public function dashboard()
    {
      $cekLogin = $this->simpleauth->check_before_login();
      // $cekLogin = $this->simpleauth->check_role_admin();

      $this->load->view('shareds/head');
      $this->load->view('shareds/header');
      $this->load->view('shareds/sidebar');
      $this->load->view('karyawan/dashboard');
      $this->load->view('shareds/footer');
    }

    public function create()
    {
      $cekLogin = $this->simpleauth->check_before_login();

      $this->load->view('shareds/head');
      $this->load->view('shareds/header');
      $this->load->view('shareds/sidebar');
      $this->load->view('karyawan/input');
      $this->load->view('shareds/footer');
    }

    public function save_data()
    {
    
    }

    public function edit_admin($id)
    {

    }

    public function update_data($id)
    {

    }

    public function publish($ket,$id='')
    {
     
    }

    public function delete_data($id)
    {

    }
  }

