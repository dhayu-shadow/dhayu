<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home extends CI_Controller {

	public function index()
	{
		$this->load->view('shareds/head');
		$this->load->view('karyawan/dashboard');
		$this->load->view('shareds/header');
		$this->load->view('shareds/sidebar');
		$this->load->view('shareds/footer');
	}
}