<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  class karyawan_model extends CI_Model
  {
    public function get_all()
    {
      $this->db->join('tbl_master','tbl_karyawan.id_jabatan=tbl_master.id_master');
      $query = $this->db->get('tbl_karyawan');
      return $query;
    }

    public function get_by_id($id)
    {
      $this->db->join('tbl_master','tbl_karyawan.id_jabatan=tbl_master.id_master');
      $this->db->where('tbl_karyawan.id',$id);
      $query = $this->db->get('tbl_karyawan');
      return $query;
    }

    public function get_by_role_admin()
    {
      $this->db->where('id_jabatan', 1);
      $query = $this->db->get('tbl_karyawan');
      return $query;
    }

    public function get_by_role_id($id)
    {
      $this->db->join('tbl_master','tbl_karyawan.id_jabatan=tbl_master.id_master');
      $this->db->where('tbl_master.id_master',$id);
      $query = $this->db->get('tbl_karyawan');
      return $query;
    }

    public function get_login($post){
      $this->db->where($post);
      $this->db->where('is_active','true');
      $login = $this->db->get('tbl_karyawan');
      return $login;
    }

    public function insert_data($data)
    {
      $insert = $this->db->insert('tbl_karyawan', $data);
      return $insert;
    }

    public function update($id,$data)
    {
      $this->db->where('id_karyawan',$id);
      $update = $this->db->update('tbl_karyawan', $data);
      return $update;
    }

    public function delete($id)
    {
      $delete = $this->db->delete('tbl_karyawan',array('id_karyawan'=>$id));
      return $delete;
    }
  }
