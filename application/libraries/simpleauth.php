<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Simpleauth
{
  var $CI;
  var $user_table = 'tbl_karyawan';


  function create($user_email = '', $user_pass = '', $data_add, $auto_login = true)
  {
    $this->CI =& get_instance();

    if($user_name == '' OR $user_pass == '') {
      return false;
    }

    $this->CI->db->where('email', $user_name);
    $query = $this->CI->db->get_where($this->user_table);

    if ($query->num_rows() > 0) 
      return false;

    $user_pass_hashed = md5($user_pass);

    $data_login = array(
          'username' => $user_name,
          'password' => $user_pass_hashed
        );

    $data = array_merge($data_login,$data_add);
    $this->CI->db->set($data);

    if(!$this->CI->db->insert($this->user_table))
      return false;

    if($auto_login)
      $this->login($user_name, $user_pass);

    return true;
  }

  function update($id = null, $user_name = '', $data, $auto_login = true)
  {
    $this->CI =& get_instance();

    if($id == null OR $user_name == '') {
      return false;
    }

    $this->CI->db->where('id_user', $id);
    $query = $this->CI->db->get_where($this->user_table);

    if ($query->num_rows() == 0){ 
      return false;
    }


    $this->CI->db->where('id_user', $id);

    if(!$this->CI->db->update($this->user_table, $data)) 
      return false;

    if($auto_login){
      $user_data['username'] = $user_name;
      $user_data['username'] = $user_data['username']; 

      $this->CI->session->set_userdata($user_data);
      }
    return true;
  }

  function login($username = '', $password = '')
  {
    $this->CI =& get_instance();

    if($username == '' OR $password == '')
      return false;


    if($this->CI->session->userdata('username') == $username)
      return true;


    $this->CI->db->where('username', $username);
    $query = $this->CI->db->get_where($this->user_table);


    if ($query->num_rows() > 0)
    {
      $user_data = $query->row_array();


      if(md5($password)!=$user_data['password'])
        return false;

      $this->CI->session->sess_destroy();

      $this->CI->session->sess_create();

      unset($user_data['password']);
      $user_data_log['id']   = $user_data['id'];
      $user_data_log['username']  = $user_data['username'];
      $user_data_log['id_jabatan']   = $user_data['id_jabatan'];
      $user_data_log['logged_in'] = true;
      $this->CI->session->set_userdata($user_data_log);

      return true;
    }else{
      return false;
    }
  }

  function check_after_login(){
    $this->CI =& get_instance();
    if($this->CI->session->userdata('logged_in')==true)
      redirect('karyawan/dashboard');
  }

  function check_before_login(){
    $this->CI =& get_instance();
    if($this->CI->session->userdata('logged_in')==false)
      redirect('karyawan/login');
  }

  function logout() {
    $this->CI =& get_instance();
    return $this->CI->session->sess_destroy();
  }

  function delete($user_id)
  {
    $this->CI =& get_instance();

    if(!is_numeric($user_id))
      return false;

    return $this->CI->db->delete($this->user_table, array('id_user' => $user_id));
  }

  function edit_password($user_name = '', $old_pass = '', $new_pass = '')
  {
    $this->CI =& get_instance();
    $this->CI->db->select('password');
    $query     = $this->CI->db->get_where($this->user_table, array('username' => $user_name));
    $user_data = $query->row_array();

    if (md5($old_pass)!=$user_data['password']){ 
      return FALSE;
    }

    $user_pass_hashed = md5($new_pass);
    $data = array(
      'password' => $user_pass_hashed
    );

    $this->CI->db->set($data);
    $this->CI->db->where('username', $user_name);
    if(!$this->CI->db->update($this->user_table, $data)){ 
      return FALSE;
    } else {
      return TRUE;
    }
  }

}
?>
