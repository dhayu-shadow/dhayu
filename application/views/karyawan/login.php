<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Aplikasi Persewaan Buku</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url('assets/css/ionicons.min.css'); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url('assets/css/AdminLTE.css'); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url('assets/css/all-skin.css'); ?>" rel="stylesheet" type="text/css" />

</head>
<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="../../index2.html"><b>Aplikasi Persewaan Buku</b></a>
      </div>
    <div class="login-box-body">
      <p class="login-box-msg">Sign in to start your session</p>
      <?php echo $this->session->flashdata('msg'); ?>
      <form action="<?php echo base_url(); ?>karyawan/do_login" method="post">
        <div class="form-group has-feedback">
          <input type="name" name="username" class="form-control" placeholder="Username">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" name="password" class="form-control" placeholder="Password">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
          <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <script src="<?php echo base_url('assets/javascript/jQuery-2.1.3.min.js'); ?>" type="text/javascript"></script>
  <script src="<?php echo base_url('assets/javascript/bootstrap.js'); ?>" type="text/javascript"></script>
</body>
</html>
