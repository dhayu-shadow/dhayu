<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Aplikasi Persewaan Buku</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
  <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url('assets/css/ionicons.min.css'); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url('assets/css/AdminLTE.css'); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url('assets/css/dataTables.bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url('assets/css/sweetalert.css'); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url('assets/css/iCheck/all.css'); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url('assets/css/all-skin.css'); ?>" rel="stylesheet" type="text/css" />

</head>