<footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
      </footer>
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url('assets/javascript/jQuery-2.1.3.min.js');?>"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url('assets/javascript/bootstrap.js');?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url('assets/javascript/jquery.slimscroll.js');?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url('assets/javascript/fastclick.js');?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url('assets/javascript/app.js');?>"></script>
  </body>
</html>
