﻿/*
Copyright (c) 2003-2011, CKSource – Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/
CKEDITOR.editorConfig = function( config )
{
	baseURL = "http://localhost/project_toyota_sukabumi/";

	config.filebrowserBrowseUrl = baseURL+ 'assets/kcfinder/browse.php?type=files';
	config.filebrowserImageBrowseUrl = baseURL+ 'assets/kcfinder/browse.php?type=image';
	config.filebrowserFlashBrowseUrl = baseURL+ 'assets/kcfinder/browse.php?type=flash';
	config.filebrowserUploadUrl = baseURL+ 'assets/kcfinder/upload.php?type=files';
	config.filebrowserImageUploadUrl = baseURL+ 'assets/kcfinder/upload.php?type=images';
	config.filebrowserFlashUploadUrl = baseURL+ 'assets/kcfinder/upload.php?type=flash';
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	config.uiColor = '#f9fafc';
	// config.skin = 'kama';
};

